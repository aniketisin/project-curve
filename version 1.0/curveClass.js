//GLOBALS
var bgcolor = '#ffffff';
var fps = 60;
var pause = false;
var game = false;
var snakes = [];
var key = [];
var screenWidth=1000,screenHeight=500;
for(i=0; i<100; i++)
    key.push(false);
var deadCount;
var initLength = 20;

function point(x,y)
{
    this.x = x;
    this.y = y;
    return this;
}

function snake(color, l, r)
{
    this.alive = true;
    this.left = l;
    this.right = r;
    this.color = color;
    this.thickness = 5;
    this.score = 0;
    this.angle = 0;
    this.speed = 2;
    this.omega = 2.3;
    this.dx = 0;
    this.dy = 0;
    this.turn_frames = 8;
    this.alpha = 1.2;
    this.time_gap = 30;
    this.countTime = 0;
    this.countFrames = 0;
    
    this.interval = 0;
    this.interval_gap = 150;
    this.gap_length = 15;
    this.current_gap = 0;
    this.gap = false;
    
    this.head_size = 3;
    this.body = [];
    return this;
}

window.requestAnimFrame = (function(callback)
{
   	return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||

    function(callback)
    {
        window.setTimeout(callback, 1000 / fps);
    };
}) ();

var X=200,Y=200,Radius=20;
function animate(s)
{
    var canvas = document.getElementById('gameDisplay');
    var context = canvas.getContext('2d');

    // update
    var n = s.body.length;
    var tempx,tempy;
    tempx = s.body[n-1].x;
    tempy = s.body[n-1].y;
    
    update(s);
    collision(canvas, context, s);

    clearhead(context, s, tempx, tempy);
    //powerups(context,s);

    // draw stuff
    if(game)
    {
    	drawSnake(context, s);
        //if(s == snakes[0])
            //drawPowerups(context);

        // request new frame
        requestAnimFrame(function()
        {
            animate(s);
        });
	}
}

function powerups(context,s)
{
    var n = s.body.length;
    if( (s.body[n-1].x - X)*(s.body[n-1].x - X) + (s.body[n-1].y - Y)*(s.body[n-1].y - Y) <= Radius*Radius )
        context.clearRect(0,0,screenWidth,screenHeight);
}

function drawPowerups(context)
{
    context.beginPath();
    context.arc(X,Y,Radius,0,2*Math.PI);
    context.fillStyle = 'green';
    context.fill();
    context.closePath();
}

function updateScores()
{
    for(i=0; i<snakes.length; i++)
        if(snakes[i].alive == true)
        {
            snakes[i].score++;
            document.getElementById(snakes[i].color).innerHTML = snakes[i].color + ": " + snakes[i].score;
        }
}

function collision(canvas, context, s)
{
	var n = s.body.length;
	var pt = context.getImageData(s.body[n-1].x+s.dx, s.body[n-1].y+s.dy, s.body[n-1].x+s.dx, s.body[n-1].y+s.dy);
    var flag = false;

    if(s.body[n-1].x <= 0 || s.body[n-1].x >= screenWidth || s.body[n-1].y <= 0 || s.body[n-1].y >= screenHeight)
        flag = true;

    if(pt.data[0] != 0 || pt.data[1] != 0 || pt.data[2] != 0 || flag)
    {
        s.alive = false;
        deadCount++;
        updateScores();
        if(deadCount == snakes.length - 1)
        {
            for(k=0; k<snakes.length; k++)
            {
                if(snakes[k].alive == true)
                {
                    alert("Winner:" + " " + snakes[k].color);
                    game = false; pause = true; s.alive = false;
                    context.clearRect(0,0,canvas.width,canvas.height);
                    break;
                }
            }
        }
    }
}

function update(s)
{
    if(!pause && s.alive)
    {
        var n = s.body.length;
        if(!s.gap)
        {
            s.interval++;
            if(s.interval == s.interval_gap)
            {
                s.interval = 0;
                s.gap = true;
            }
        }
        else
        {
            s.current_gap++;
            if(s.current_gap == s.gap_length)
            {
                s.gap = false;
                s.current_gap = 0;
                var n = s.body.length;
                var temp1 = new point(s.body[n-1].x,s.body[n-1].y);
                var temp2 = new point(s.body[n-1].x,s.body[n-1].y);
                s.body = [];
                s.body.push(temp1);
                s.body.push(temp2);
            }
        }

	    if(key[s.left])
	    { 
	    	s.angle += s.omega;
            if(s.countTime <= s.time_gap && s.countFrames <= s.turn_frames)
            {
                s.angle += (s.alpha-1)*s.omega;
                s.countFrames++;
            }
            var n = s.body.length;
            var temp = new point(s.body[n-1].x,s.body[n-1].y);
            s.body.push(temp);
	    }
	    else if(key[s.right])
	    { 
	    	s.angle -= s.omega;
            if(s.countTime <= s.time_gap && s.countFrames <= s.turn_frames)
            {
                s.angle -= (s.alpha-1)*s.omega;
                s.countFrames++;
            }
            var n = s.body.length;
            var temp = new point(s.body[n-1].x,s.body[n-1].y);
            s.body.push(temp);
	    }    
    
        if(!s.countFrames)
            s.countTime++;
        
        var speedX,speedY;
        var factor = 1;

        speedX = s.speed*Math.cos(s.angle/180*Math.PI)*factor;
        speedY = -1*s.speed*Math.sin(s.angle/180*Math.PI)*factor;
        s.dx = 3*Math.cos(s.angle/180*Math.PI);
        s.dy = -3*Math.sin(s.angle/180*Math.PI);

        n = s.body.length;
        s.body[n-1].x += speedX; 
        s.body[n-1].y += speedY;
    }
}

function drawSnake(context, s)
{
    //body
    var n;
    if(!s.gap)
    {
	    context.beginPath();
        context.lineJoin = 'round';
        context.lineWidth = s.thickness;
        n = s.body.length;
        context.moveTo(s.body[0].x,s.body[0].y);
        for(j=1; j<s.body.length; j++)
        {
            context.lineTo(s.body[j].x,s.body[j].y);
        }
        context.strokeStyle = s.color;
        context.stroke();
        context.closePath();
    }

    //head
    context.beginPath();
    context.fillStyle = "0x000000";
    n = s.body.length;
    context.arc(s.body[n-1].x,s.body[n-1].y,s.head_size,0,2*Math.PI);
    context.fill();
    context.closePath();
}

function clearhead(context, s, tempx, tempy)
{
    var n = s.body.length;
    var l = s.head_size;
    context.clearRect(tempx-l-1,tempy-l-1,2*l+2,2*l+2);
}

function main()
{
    document.addEventListener('keydown',
    function(e)
    {
    	if(game)
    	{
    		if(e.keyCode == 32 && pause == false){ pause = true; return; }
    		else if(e.keyCode == 32 && pause == true){ pause = false; return; }
    		else if(pause)return;
    	
    		key[e.keyCode] = true;
    	}
    	else
    	{
    		if(e.keyCode == 32)
    		{
    			startgame();
    		}
    	}
    });
    
    document.addEventListener('keyup',
    function(e)
    {
    	if(game)
    	{
    		key[e.keyCode] = false;
    	}
    });

    //snakes = new Array();
    var tempsnake = new snake('red', 37, 39);
    snakes.push(tempsnake);
    tempsnake = new snake('blue',90, 88);
    snakes.push(tempsnake);
    for(i=0;i<snakes.length;i++)
    {
        $('#score').append("<li id="+snakes[i].color+">"+snakes[i].color + ": "+snakes[i].score + "</li>");
    }
    if(pause)
        alert("PAUSED");
}

function startgame()
{

    game = true;
    deadCount = 0;
    pause = false;
    key = [];
    for(i=0; i<100; i++)
        key.push(false);
    for(i=0; i<snakes.length; i++)
    {
    	snakes[i].alive = true;
    	//console.log(snakes[i].body.length);
    	while(snakes[i].body.length)
    		snakes[i].body.pop();
    	
    	var x1,x2,y1,y2;
    	x1 = screenWidth/10 + Math.random()*screenWidth*8/10;
    	y1 = screenHeight/10 + Math.random()*screenHeight*8/10;
    	snakes[i].angle = Math.random()*360;

    	x2 = x1 + initLength*Math.cos(Math.PI/180*snakes[i].angle);
    	y2 = y1 - initLength*Math.sin(Math.PI/180*snakes[i].angle);

    	snakes[i].body.push(new point(x1,y1));
    	snakes[i].body.push(new point(x2,y2));

    	animate(snakes[i]);
    }
    //updateScores();
}
main();