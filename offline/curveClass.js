//GLOBALS
var fps = 30;
var pause = false;
var game = false;
var snakes = [];
var key = [];
var currentPowers = [];
var screenWidth=1000,screenHeight=500;
for(var i=0; i<100; i++)
    key.push(false);
var deadCount;
var initLength = 10;
var mindist = 50;

//-------------------------------------------
//Power Globals
var numberOfPowers = 7;
var defaultHeadColor = 'black';
var reverseKeyHeadColor = 'yellow';
var collisionHeadColor = 'green';
var nextPowerFrames = 400;
var nextPowerCountFrames = 0;
var powerColors = ['orange','magenta','green','black','yellow','pink','brown'];
var cumulativeProbability = [25,40,55,75,85,95,100];
/*
	power types:

	0 = clear screen                       ---     25%     25
	1 = half your speed                    ---     15%     40
	2 = half your thickness                ---     15%     55
	3 = allow collision for you            ---     20%     75
    4 = double everyone else's speed       ---     10%     85
    5 = double everyone else's thickness   ---     10%     95
    6 = reverse everyone else's keys       ---     5%      100
*/
//-------------------------------------------

function point(x,y)
{
    this.x = x;
    this.y = y;
    return this;
}

function power(x,y,type)
{
	this.x = x;
	this.y = y;
	this.radius = 10;
	this.type = type;
	this.currentFrame = 0;
	this.frame = 20*fps;
    this.color = powerColors[type];
    return this;
}

function snake(color, l, r)
{
    this.alive = true;
    this.left = l;
    this.right = r;
    this.color = color;
    this.headColor = defaultHeadColor;
    this.headRadius = 4;
    this.thickness = 5;
    this.score = 0;
    this.angle = 0;
    this.speed = 1.5;
    this.omega = 2;
    this.dx = 0;
    this.dy = 0;

    //increase omega when reversing turn
    this.turn_frames = 8;
    this.alpha = 1.2;
    this.time_gap = 30;
    this.countTime = 0;
    this.countFrames = 0;
    
    //gaps in snakes
    this.interval = 0;
    this.interval_gap = 150;
    this.gap_length = 15;
    this.current_gap = 0;
    this.gap = false;

    //powers
    this.powerbool = [];
    this.powerCountFrames = [];
    this.powerFrames = []
    for(var i=0; i<numberOfPowers; i++)
    {
    	this.powerbool.push(false);
        this.powerCountFrames.push(0);
        this.powerFrames.push(20*fps);
    }
    
    this.body = [];
    return this;
}

window.requestAnimFrame = (function(callback)
{
   	return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||

    function(callback)
    {
        window.setTimeout(callback, 100);
    };
}) ();

function animate(s)
{
    var bodycanvas = document.getElementById('body');
    var bodycontext = bodycanvas.getContext('2d');
    var powercanvas = document.getElementById('powers');
    var powercontext = powercanvas.getContext('2d');
    var headcanvas = document.getElementById('head');
    var headcontext = headcanvas.getContext('2d');

    var n = s.body.length;
    
    if(!pause && s.alive)
    {
    	update(s);
        if(s == snakes[0])
            updatePowerFrames();
        checkPowers(bodycontext, powercanvas, powercontext, s);
    	collision(bodycanvas, bodycontext, s);
        if(s == snakes[0])
            headcontext.clearRect(0,0,screenWidth,screenHeight);
	}
    
    if(game)
    {
        drawSnake(bodycontext, headcontext, s);
        if(s == snakes[0])
        {
            powercontext.clearRect(0,0,screenWidth,screenHeight);
            drawPowerups(powercontext);
        }

        requestAnimFrame(function()
        {
            animate(s);
        });
	}
}

function addPower()
{
    var p = Math.random()*100,type;
    for(var i=0; i<cumulativeProbability.length; i++)
    {
        if(p<=cumulativeProbability[i])
        {
            type = i;
            break;
        }
    }

    var x = screenWidth/10 + Math.random()*screenWidth*8/10;
    var y = screenHeight/10 + Math.random()*screenHeight*8/10;
    currentPowers.push(new power(x,y,type) );
}

function removePower(i)
{
    var n = currentPowers.length;
    currentPowers.splice(i,1);
    console.log(n + " " + currentPowers.length)
}

function updatePowerFrames()
{
    nextPowerCountFrames++;
    if(nextPowerCountFrames == nextPowerFrames)
    {
        nextPowerCountFrames = 0;
        addPower();
    }
    for(var i=0; i<currentPowers.length; i++)
    {
        currentPowers[i].currentFrame++;
        if(currentPowers[i].currentFrame == currentPowers[i].frame)
            removePower(i);
    }
}

function checkPowers(bodycontext,canvas,context,s)
{
    var n = s.body.length,dist;
    var tempx = s.body[n-1].x, tempy = s.body[n-1].y;

    for(var i=0; i<currentPowers.length; i++)
    {
        dist = (tempx - currentPowers[i].x)*(tempx - currentPowers[i].x);
        dist += (tempy - currentPowers[i].y)*(tempy - currentPowers[i].y);
        if(dist <= currentPowers[i].radius*currentPowers[i].radius)
        {
            if(currentPowers[i].type == 0)
            {
                for(var j=0; j<snakes.length; j++)
                {
                	var n = snakes[j].body.length;
                	var temp = new point(snakes[j].body[n-1].x,snakes[j].body[n-1].y);
                	var temp1 = new point(snakes[j].body[n-1].x,snakes[j].body[n-1].y);
                	snakes[j].body = [];
                	snakes[j].body.push(temp1);
                	snakes[j].body.push(temp);
                	snakes[j].interval = 0;
            		snakes[j].countTime = 0;
            		snakes[j].countFrames = 0;
            		snakes[j].current_gap = 0;
            		snakes[j].gap = false;
                }
                context.clearRect(0,0,screenWidth,screenHeight);
                bodycontext.clearRect(0,0,screenWidth,screenHeight);
            }
            else if(currentPowers[i].type == 1)
            {
                if(s.powerbool[1])
                {
                    s.powerCountFrames[1] = 0;
                    continue;
                }
                s.powerbool[1] = true;
                s.speed /= 2;
            }
            else if(currentPowers[i].type == 2)
            {
                if(s.powerbool[2])
                {
                    s.powerCountFrames[2] = 0;
                    continue;
                }
                s.powerbool[2] = true;
                s.thickness /= 2;
                s.headRadius /= 2;
            }
            else if(currentPowers[i].type == 3)
            {
                if(s.powerbool[3])
                {
                    s.powerCountFrames[3] = 0;
                    continue;
                }
                s.powerbool[3] = true;
                s.headColor = collisionHeadColor;
            }
            else if(currentPowers[i].type == 4)
            {
                var flag = false,idx;
                s.powerbool[4] = true;
                for(var j=0; j<snakes.length; j++)
                {
                    if(snakes[j] != s && snakes[j].powerbool[4])
                    {
                        flag = true;
                        idx = j;
                        break;
                    }
                }
                if(flag)
                {
                    snakes[idx].speed *= 2;
                    snakes[idx].powerbool[4] = false;
                    snakes[idx].powerCountFrames[4] = 0;
                    s.speed /= 2;
                    continue;
                }
                for(var j=0; j<snakes.length; j++)
                {
                    if(snakes[j] != s)
                        snakes[j].speed *= 2;
                }
            }
            else if(currentPowers[i].type == 5)
            {
                var flag = false,idx;
                s.powerbool[5] = true;
                for(var j=0; j<snakes.length; j++)
                {
                    if(snakes[j] != s && snakes[j].powerbool[5])
                    {
                        flag = true;
                        idx = j;
                        break;
                    }
                }
                if(flag)
                {
                    snakes[idx].thickness *= 2;
                    snakes[idx].powerbool[5] = false;
                    snakes[idx].powerCountFrames[5] = 0;
                    snakes[idx].headRadius *= 2;
                    var n = snakes[idx].body.length;
                    var temp = new point(snakes[idx].body[n-1].x,snakes[idx].body[n-1].y);
                    var temp1 = new point(snakes[idx].body[n-1].x,snakes[idx].body[n-1].y);
                    snakes[idx].body = [];
                    snakes[idx].body.push(temp1);
                    snakes[idx].body.push(temp);
                    
                    s.thickness /= 2;
                    s.headRadius *= 2;
                    var n = s.body.length;
                    var temp = new point(s.body[n-1].x,s.body[n-1].y);
                    var temp1 = new point(s.body[n-1].x,s.body[n-1].y);
                    s.body = [];
                    s.body.push(temp1);
                    s.body.push(temp);
                    continue;
                }
                for(var j=0; j<snakes.length; j++)
                {
                    if(snakes[j] != s)
                    {
                        snakes[j].thickness *= 2;
                        snakes[j].headRadius *= 2;
                        var n = snakes[j].body.length;
                        var temp = new point(snakes[j].body[n-1].x,snakes[j].body[n-1].y);
                        var temp1 = new point(snakes[j].body[n-1].x,snakes[j].body[n-1].y);
                        snakes[j].body = [];
                        snakes[j].body.push(temp1);
                        snakes[j].body.push(temp);
                    }
                }
            }
            else if(currentPowers[i].type == 6)
            {
                var flag = false,idx;
                s.powerbool[6] = true;
                for(var j=0; j<snakes.length; j++)
                {
                    if(snakes[j] != s && snakes[j].powerbool[6])
                    {
                        flag = true;
                        idx = j;
                        break;
                    }
                }
                if(flag)
                {
                    snakes[idx].headColor = reverseKeyHeadColor;
                    var tempkey = snakes[idx].left;
                    snakes[idx].left = snakes[idx].right;
                    snakes[idx].right = tempkey;
                    snakes[idx].powerbool[6] = false;
                    snakes[idx].powerCountFrames[6] = 0;

                    s.headColor = defaultHeadColor;
                    var tempkey = s.left;
                    s.left = s.right;
                    s.right = tempkey;
                    continue;
                }
                for(var j=0; j<snakes.length; j++)
                {
                    if(snakes[j] != s)
                    {
                        snakes[j].headColor = reverseKeyHeadColor;
                        var tempkey = snakes[j].left;
                        snakes[j].left = snakes[j].right;
                        snakes[j].right = tempkey;
                    }
                }   
            }
            removePower(i);
        }
    }
}

function updatePowers(s)
{
    for(var i=0; i<s.powerbool.length; i++)
    {
        if(!s.powerbool[i])
            continue;
        s.powerCountFrames[i]++;
        if(s.powerCountFrames[i] == s.powerFrames[i])
        {
            s.powerCountFrames[i] = 0;
            s.powerbool[i] = false;
            if(i==1)
            {
                s.speed *= 2;
            }
            else if(i==2)
            {
                s.thickness *= 2;
                s.headRadius *= 2;
                var n = s.body.length;
                var temp = new point(s.body[n-1].x,s.body[n-1].y);
                var temp1 = new point(s.body[n-1].x,s.body[n-1].y);
                s.body = [];
                s.body.push(temp1);
                s.body.push(temp);
            }
            else if(i==3)
            {
                s.headColor = defaultHeadColor;
                continue;
            }
            else if(i==4)
            {
                for(var j=0; j<snakes.length; j++)
                {
                    if(snakes[j] != s)
                        snakes[j].speed /= 2;
                }
            }
            else if(i==5)
            {
                for(var j=0; j<snakes.length; j++)
                {
                    if(snakes[j] != s)
                    {
                        snakes[j].thickness /= 2;
                        snakes[j].headRadius /= 2;
                        var n = snakes[j].body.length;
                        var temp = new point(snakes[j].body[n-1].x,snakes[j].body[n-1].y);
                        var temp1 = new point(snakes[j].body[n-1].x,snakes[j].body[n-1].y);
                        snakes[j].body = [];
                        snakes[j].body.push(temp1);
                        snakes[j].body.push(temp);
                    }
                }
            }
            else if(i==6)
            {
                for(var j=0; j<snakes.length; j++)
                {
                    if(snakes[j] != s)
                    {
                        s.headColor = defaultHeadColor;
                        var tempkey = snakes[j].left;
                        snakes[j].left = snakes[j].right;
                        snakes[j].right = tempkey;  
                    }
                }
            }
        }
    }
}

function drawPowerups(context)
{
    for(var i=0; i<currentPowers.length; i++)
    {
        context.beginPath();
        context.arc(currentPowers[i].x,currentPowers[i].y,currentPowers[i].radius,0,2*Math.PI);
        context.fillStyle = currentPowers[i].color;
        context.fill();
        context.closePath();
    }
}

function updateScores()
{
    for(var i=0; i<snakes.length; i++)
        if(snakes[i].alive == true)
        {
            snakes[i].score++;
            document.getElementById(snakes[i].color).innerHTML = snakes[i].color + ": " + snakes[i].score;
        }
}

function collision(canvas, context, s)
{
	var n = s.body.length;
	var pt = context.getImageData(s.body[n-1].x+s.dx, s.body[n-1].y+s.dy, s.body[n-1].x+s.dx, s.body[n-1].y+s.dy);
    var flag = false;

    if(s.body[n-1].x <= 0 || s.body[n-1].x >= screenWidth || s.body[n-1].y <= 0 || s.body[n-1].y >= screenHeight)
    {
        if(s.powerbool[3])
        {
            if(s.body[n-1].x <= 0)
            {
                var temp = s.body[n-1];
                s.body = [];
                s.body.push( new point(screenWidth,temp.y) );
                s.body.push( new point(screenWidth,temp.y) );
            }
            else if(s.body[n-1].x >= screenWidth)
            {
                var temp = s.body[n-1];
                s.body = [];
                s.body.push( new point(0,temp.y) );
                s.body.push( new point(0,temp.y) );
            }
            else if(s.body[n-1].y <= 0)
            {
                var temp = s.body[n-1];
                s.body = [];
                s.body.push( new point(temp.x,screenHeight) );
                s.body.push( new point(temp.x,screenHeight) );
            }
            else if(s.body[n-1].y >= screenHeight)
            {
                var temp = s.body[n-1];
                s.body = [];
                s.body.push( new point(temp.x,0) );
                s.body.push( new point(temp.x,0) );
            }
        }
        else
            flag = true;
    }

    if(s.powerbool[3])
        return;

    if(pt.data[0] != 0 || pt.data[1] != 0 || pt.data[2] != 0 || flag)
    {
        s.alive = false;
        deadCount++;
        updateScores();
        if(deadCount == snakes.length - 1)
        {
            for(var k=0; k<snakes.length; k++)
            {
                if(snakes[k].alive == true)
                {
                    game = false; pause = true; s.alive = false;
                    //context.clearRect(0,0,canvas.width,canvas.height);
                    break;
                }
            }
        }
    }
}

function update(s)
{
    if(!pause && s.alive)
    {
        var n = s.body.length;

        //---------------------------------------------
        //Powerups
        updatePowers(s);
        //---------------------------------------------


        //---------------------------------------------
        //Gaps in snake
        if(!s.gap)
        {
            s.interval++;
            if(s.interval == s.interval_gap)
            {
                s.interval = 0;
                s.gap = true;
            }
        }
        else
        {
            s.current_gap++;
            if(s.current_gap == s.gap_length)
            {
                s.gap = false;
                s.current_gap = 0;
                var n = s.body.length;
                var temp1 = new point(s.body[n-1].x,s.body[n-1].y);
                var temp2 = new point(s.body[n-1].x,s.body[n-1].y);
                s.body = [];
                s.body.push(temp1);
                s.body.push(temp2);
            }
        }
        //---------------------------------------------


        //---------------------------------------------
        //Fast turning
	    if(key[s.left])
	    { 
	    	s.angle += s.omega;
            if(s.countTime <= s.time_gap && s.countFrames <= s.turn_frames)
            {
                s.angle += (s.alpha-1)*s.omega;
                s.countFrames++;
            }
            var n = s.body.length;
            var temp = new point(s.body[n-1].x,s.body[n-1].y);
            s.body.push(temp);
	    }
	    else if(key[s.right])
	    { 
	    	s.angle -= s.omega;
            if(s.countTime <= s.time_gap && s.countFrames <= s.turn_frames)
            {
                s.angle -= (s.alpha-1)*s.omega;
                s.countFrames++;
            }
            var n = s.body.length;
            var temp = new point(s.body[n-1].x,s.body[n-1].y);
            s.body.push(temp);
	    }        
        if(!s.countFrames)
            s.countTime++;
        //---------------------------------------------


        //---------------------------------------------        
        //Change position
        var speedX,speedY;
        var factor = 1;

        speedX = s.speed*Math.cos(s.angle/180*Math.PI)*factor;
        speedY = -1*s.speed*Math.sin(s.angle/180*Math.PI)*factor;
        s.dx = 3*Math.cos(s.angle/180*Math.PI);
        s.dy = -3*Math.sin(s.angle/180*Math.PI);

        n = s.body.length;
        s.body[n-1].x += speedX; 
        s.body[n-1].y += speedY;
        //---------------------------------------------
    }
}

function drawSnake(context, headcontext, s)
{
    //body
    var n;
    if(!s.gap)
    {
	    context.beginPath();
        context.lineJoin = 'round';
        context.lineWidth = s.thickness;
        n = s.body.length;
        context.moveTo(s.body[0].x,s.body[0].y);
        for(var j=1; j<s.body.length; j++)
        {
            context.lineTo(s.body[j].x,s.body[j].y);
        }
        context.strokeStyle = s.color;
        context.stroke();
        context.closePath();
    }

    //head
    headcontext.beginPath();
    headcontext.fillStyle = s.headColor;
    n = s.body.length;
    headcontext.arc(s.body[n-1].x,s.body[n-1].y,s.headRadius,0,2*Math.PI);
    headcontext.fill();
    headcontext.closePath();
}

function main()
{
    document.addEventListener('keydown',
    function(e)
    {
    	if(game)
    	{
    		if(e.keyCode == 32 && pause == false){ pause = true; return; }
    		else if(e.keyCode == 32 && pause == true){ pause = false; return; }
    		else if(pause)return;
    	
    		key[e.keyCode] = true;
    	}
    	else
    	{
    		if(e.keyCode == 32)
    		{
    			startgame();
    		}
    	}
    });
    
    document.addEventListener('keyup',
    function(e)
    {
    	if(game)
    	{
    		key[e.keyCode] = false;
    	}
    });

    var tempsnake = new snake('red', 37, 39);
    snakes.push(tempsnake);
    tempsnake = new snake('blue',90, 88);
    snakes.push(tempsnake);
    for(var i=0;i<snakes.length;i++)
    {
        $('#score').append("<li id="+snakes[i].color+">"+snakes[i].color + ": "+snakes[i].score + "</li>");
    }
    if(pause)
        alert("PAUSED");
}

function startgame()
{
	var canvas = document.getElementById("body");
	var context = canvas.getContext('2d');
    var powercanvas = document.getElementById('powers');
    var powercontext = powercanvas.getContext('2d');
    var headcanvas = document.getElementById('head');
    var headcontext = headcanvas.getContext('2d');
    screenWidth = canvas.width;
    screenHeight = canvas.height;
	context.clearRect(0,0,canvas.width,canvas.height);
    powercontext.clearRect(0,0,canvas.width,canvas.height);
    headcontext.clearRect(0,0,canvas.width,canvas.height);
    game = true;
    deadCount = 0;
    pause = true;
    key = [];
    nextPowerCountFrames = 0;
    currentPowers = [];
    for(var i=0; i<100; i++)
        key.push(false);
    for(var i=0; i<snakes.length; i++)
    {
    	snakes[i].alive = true;
    	snakes[i].interval = 0;
    	snakes[i].countTime = 0;
    	snakes[i].countFrames = 0;
    	snakes[i].current_gap = 0;
    	snakes[i].gap = false;

        for(var j=0; j<snakes[i].powerbool.length; j++)
        {
            snakes[i].powerCountFrames[j] = 0;
            if(snakes[i].powerbool[j])
            {
                if(j==1)
                {
                    snakes[i].speed *= 2;
                }
                else if(j==2)
                {
                    snakes[i].headRadius *= 2;
                    snakes[i].thickness *= 2;
                }
                else if(j==3)
                {
                    snakes[i].headColor = defaultHeadColor;
                }
                else if(j==4)
                {
                    for(var k=0; k<snakes.length; k++)
                    {
                        if(snakes[k] != snakes[i])
                            snakes[k].speed /= 2;
                    }
                }
                else if(j==5)
                {
                    for(var k=0; k<snakes.length; k++)
                    {
                        if(snakes[k] != snakes[i])
                        {
                            snakes[k].thickness /= 2;
                            snakes[k].headRadius /= 2;
                        }
                    }
                }
                else if(j==6)
                {
                    for(var k=0; k<snakes.length; k++)
                    {
                        if(snakes[k] != snakes[i])
                        {
                            snakes[k].headColor = defaultHeadColor;
                            var tempkey = snakes[k].left;
                            snakes[k].left = snakes[k].right;
                            snakes[k].right = tempkey;  
                        }
                    }
                }
            }
            snakes[i].powerbool[j] = false;
        }

    	while(snakes[i].body.length)
    		snakes[i].body.pop();

    	var loop=true;
    	while(loop)
    	{
    		loop = false;
    		snakes[i].body = initPos(i);
    		for(var j=0; j<i; j++)
    		{
    			if(posCompare(i,j))
    			{
    				loop = true;
    				break;
    			}
    		}
    	}
    	animate(snakes[i]);
    }
}

function posCompare(i,j)
{
	for(var x=0; x<2; x++)
	{
		for(var y=0; y<2; y++)
		{
			var dist = (snakes[i].body[x].x-snakes[j].body[y].x)*(snakes[i].body[x].x-snakes[j].body[y].x);
			dist += (snakes[i].body[x].y-snakes[j].body[y].y)*(snakes[i].body[x].y-snakes[j].body[y].y);
			if(dist < mindist*mindist)
				return true;
		}
	}
	return false;
}

function initPos(i)
{
	var x1,x2,y1,y2,temp = [];
	x1 = screenWidth/10 + Math.random()*screenWidth*8/10;
	y1 = screenHeight/10 + Math.random()*screenHeight*8/10;
	snakes[i].angle = Math.random()*360;

	x2 = x1 + initLength*Math.cos(Math.PI/180*snakes[i].angle);
	y2 = y1 - initLength*Math.sin(Math.PI/180*snakes[i].angle);

	temp.push(new point(x1,y1));
	temp.push(new point(x2,y2));
	return temp;
}
main();